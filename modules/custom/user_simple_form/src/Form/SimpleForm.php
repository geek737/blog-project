<?php

namespace Drupal\user_simple_form\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Our simple form class.
 */
class SimpleForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_simple_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['firstName'] = [
      '#type' => 'textfield',
      '#title' => $this->t('First Name'),
    ];

    $form['lastName'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Last Name'),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Display'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->messenger()->addStatus($this->t('Your first name : ' . $form_state->getValue('firstName') . ' </br> Your last name : '. $form_state->getValue('lastName')));
  }

}
